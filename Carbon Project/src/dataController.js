export const headerData = [
  {
    header: 'ID',
    key: 'id',
  },
  {
    header: 'Task To Do',
    key: 'ToDO',
  },
  {
    header: 'Status',
    key: 'Status',
  },
  {
    header: 'Complete Task',
    key: 'Completed',
  },
  {
    header: 'Delete Task',
    key: 'Delete',
  },
];
