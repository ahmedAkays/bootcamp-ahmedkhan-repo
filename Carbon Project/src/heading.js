import React from "react";
import './heading.scss';

export const Heading = (props) => {
    return (
        <h1 className=".info-section__heading" style={{textAlign: "center", paddingBottom: "25px"}}>{props.text}</h1>
    );
  };

  export const Label = (props) => {
    return (
        <h4 className=".info-section__heading">{props.text}</h4>
    );
  };
