import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import { ToDoListTable } from './components/dataTable';
import * as URLs from './constants';
import { Heading, Label } from './heading';

import { Button, TextInput } from 'carbon-components-react';

class App extends Component {
  constructor() {
    super();
    this.state = { rowData: [], newNote: '' };
  }
  componentDidMount() {
    fetch(URLs.getTODOs, {
      method: 'get',
      headers: new Headers({
        'Content-Type': 'application/json',
        'x-Gateway-ApiKey': URLs.GUID,
        'csrf-token': '',
      }),
    })
      .then(res => {
        return res.json();
      })
      .then(toDoList => {
        toDoList.map(todo => (todo.id = String(todo.id)));
        this.setState({ rowData: toDoList });
        console.log(this.state.rowData);
      });
  }
  submitNewTask() {
    const newNote = document.getElementById('newToDoTextInput').value;
    alert(newNote);
    if (newNote === '') {
      alert('Note cannot be emptry.');
    } else {
      fetch(URLs.postTODO, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'x-Gateway-ApiKey': URLs.GUID,
          'csrf-token': 'abcd',
        },
        body: JSON.stringify({ note: '"' + newNote + '"' }),
      }).then(res => {
        console.log(res);
        window.location.reload(false);
      });
    }
  }

  render() {
    return (
      <div className="App">
        <Heading text="To Do App"> </Heading>
        <Label text="New Note:" />
        <TextInput labelText="" id="newToDoTextInput" /> <br />
        <Button id="submitButton" onClick={this.submitNewTask.bind(this)}>
          Submit Task
        </Button>
        <br />
        <ToDoListTable dataList={this.state.rowData} />
      </div>
    );
  }
}

export default App;
