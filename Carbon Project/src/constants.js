export const getTODOs = 'http://localhost:81/todoApp/api/todos';

//export const postTODO = "https://cors-anywhere.herokuapp.com/http://localhost:81/todoApp/api/todo";

export const postTODO = 'http://localhost:81/todoApp/api/todo';

export const deleteTODO = 'http://localhost:81/todoApp/api/todo';

export const updateTODO = 'http://localhost:81/todoApp/api/todo';

export const GUID = '123e4567-e89b-12d3-a456-426614174000';
