import React from "react";
import {
    Button,
    DataTable,
    TableContainer,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Table, 
    TableHeader,
  } from 'carbon-components-react';
import { headerData } from '../dataController';
import * as URLs from '../constants';


export const ToDoListTable = (props) => {
    return (
        <div>
        <DataTable
          rows={props.dataList}
          headers={headerData}
          render={({ rows, headers, getHeaderProps }) => (
            <TableContainer title="To Do List">
              <Table>
                <TableHead>
                  <TableRow>
                    {headers.map(header => (
                      <TableHeader {...getHeaderProps({ header })}>
                        {header.header}
                      </TableHeader>
                    ))}
                  </TableRow>
                </TableHead>

                <TableBody>
                  {props.dataList.map(row => (
                    <TableRow key={ String(row.id)}>
                      <TableCell key={String(row.id)} style={{width: "10%"}}>{String(row.id)}</TableCell>
                      <TableCell style={{width: "25%"}}>{row.note}</TableCell>
                      <TableCell style={{width: "25%"}}>{row.status}</TableCell>
                      <TableCell style={{width: "20%"}} >        
                        <Button 
                          id={String(row.id)} 
                          value={String(row.id)} 
                          onClick={e => {
                            const id = e.target.value;
                            console.log(id);
                            fetch(URLs.updateTODO, {
                              method: 'PATCH',
                              headers: {
                                'Content-Type': 'application/json',
                                'x-Gateway-ApiKey': URLs.GUID,
                                'csrf-token': 'abcd',
                              },
                              body: JSON.stringify( { id : "\"" + id + "\""})   
                            }).then(res => {
                              console.log(res);
                              window.location.reload(false);
                            });

                          }} >
                            Mark Done
                        </Button>
                      </TableCell>
                      
                      <TableCell style={{width: "20%"}} >        
                        <Button 
                          id={String(row.id)} 
                          value={String(row.id)} 
                          onClick={e => {
                            const id = e.target.value;
                            console.log(id);
                            fetch(URLs.deleteTODO, {
                              method: 'DELETE',
                              headers: {
                                'Content-Type': 'application/json' ,
                                'x-Gateway-ApiKey': URLs.GUID,
                                'csrf-token': 'abcd',
                              },
                              body: JSON.stringify( { id : "\"" + id + "\""})   
                            }).then(res => {
                              console.log(res);
                              window.location.reload(false);
                            });

                          }} >
                            Delete Task
                        </Button>
                      </TableCell>
                      

                      

                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          )}
        />
        </div>

    )};


