import * as express from 'express';
import * as fs from 'fs';

export function loggerMiddleware(request: express.Request, response: express.Response, next) {
    response.header('Access-Control-Allow-Origin', '*'); //replace localhost with actual host
    response.header('Access-Control-Allow-Methods', 'OPTIONS, GET, PUT, PATCH, POST, DELETE');
    response.header('Access-Control-Allow-Headers', 'Content-Type, X-Requested-With, Authorization, x-Gateway-ApiKey, csrf-token');


    
    const dataToAppend = JSON.stringify( 
        { 
            timestamp: new Date(), 
            method: request.method, 
            path: request.path,
            headers: {
                "x-Gateway-ApiKey" : request.header('x-Gateway-ApiKey')  != null? request.header('x-Gateway-ApiKey') : "",
                "csrf-token" :  request.header('csrf-token') != null? request.header('csrf-token') : ""
            }  
        } 
    ) + "\n";
    
    fs.appendFile('logger.txt', dataToAppend, function (err) {
        if (err) throw err;
        console.log('Saved!');
    });

    if ("/todoApp/api/health" == request.path){
        next();
    }
    else {
        console.log(`First Middleware: ${request.method} ${request.path}`);
        next();
    }
}
export function headerCheckMiddleware(request: express.Request, response: express.Response, next){
    if ("/todoApp/api/health" == request.path){
        next();
    }
    else if (request.header('x-Gateway-ApiKey') != null && request.header('csrf-token') != null ){
        console.log(`Second Middleware: `);
        console.log(`Header x-Gateway-ApiKey:  ${request.header('x-Gateway-ApiKey')}`);
        console.log(`Header csrf-token:  ${request.header('csrf-token')}`);
        next();
    }
    else {
        console.log("Headers Not Present");
        response.send("Headers Not Present");
    }

}
