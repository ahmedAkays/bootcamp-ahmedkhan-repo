import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mysql from './mysqlInterface';
import * as middleware from './middleware';
import * as fs from 'fs';

 
const app = express();

// create application/json parser
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.use(bodyParser.json());


app.use(middleware.loggerMiddleware);
app.use(middleware.headerCheckMiddleware);

function log(requestText, responseText){
    const dataToAppend = JSON.stringify( 
        { 
            timestamp: new Date(), 
            request: requestText,
            response: responseText
        } 
    ) + "\n";
    
    fs.appendFile('logger.txt', dataToAppend, function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
}

app.get('/todoApp/api/todos', urlencodedParser, (request, response) => {
    var sql = "select * from Bootcamp.notes";
    mysql.con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Result: " + result);
        console.log(result);
        log("",  result);
        response.send(result);
      });

});

app.post('/todoApp/api/todo', urlencodedParser, (request, response) => {
    console.log(request.body);

//    var param = JSON.parse(request.body.note);
    var newNote = JSON.parse(request.body.note);
    var sql = "insert into Bootcamp.notes (note, createdAt, status) values ('" + newNote + "', now(), 'Incomplete' )";
    mysql.con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("One record inserted");
        log( {note: JSON.parse(request.body.note) } ,  "One record inserted");
        response.send("One record inserted");
      });


});
 
app.patch('/todoApp/api/todo', urlencodedParser,  (request, response) => {
    console.log(request.body);

    var id = JSON.parse(request.body.id);
    var sql = "update Bootcamp.notes set Bootcamp.notes.status = 'Completed' where Bootcamp.notes.id = " + id;
    mysql.con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("One record updated");
        log( {id: JSON.parse(request.body.id) } ,  "One record updated");

        response.send("One record updated");
      });

});

app.delete('/todoApp/api/todo', urlencodedParser,  (request, response) => {
    console.log(request.body);

    var id = JSON.parse(request.body.id);
    var sql = "delete from Bootcamp.notes where Bootcamp.notes.id = " + id;
    mysql.con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("One record deleted");
        log( {id: JSON.parse(request.body.id) } ,  "One record deleted");

        response.send("One record deleted");
      });

});

app.get('/todoApp/api/health', (request, response) => {
    log( "" ,  {"Status": 200});
    response.send({"Status": 200});
});


app.listen(81);

